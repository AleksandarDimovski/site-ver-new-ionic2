import { Component, ViewChild } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Storage } from '@ionic/storage';
import { AlertController } from 'ionic-angular';
import { App } from 'ionic-angular';
import { Events } from 'ionic-angular';
import _ from "underscore";

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  
  user: string = '';

  @ViewChild(Nav) nav: Nav;

  rootPage: string = 'LoginPage';

  pages: Array<{title: string, component: any}>;


  constructor(public platform: Platform, 
    public statusBar: StatusBar, 
    public splashScreen: SplashScreen, 
    private storage: Storage,
    private alertCtrl: AlertController,
    private app:App,
    public events: Events,
  ) {
    this.initializeApp();

    this.events.subscribe('user-is-logged', (data) => {
      console.log("data",data)
      // user and time are the same arguments passed in `events.publish(user, time)`
      this.user = data.display_name;
      console.log(this.user = data.display_name);
    });

    // used for an example of ngFor and navigation
    //this.pages = [
      //{ title: 'Home', component: HomePage }
    //];
    //

  }

  initializeApp() {
    
    this.platform.ready().then(() => {

     
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
     
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
   
   
    
  }

  logout() { 
    let alert = this.alertCtrl.create({
      title: 'Confirm Log Out',
      message: 'Are you sure you want to log out?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Log Out',
          handler: () => {
            console.log('Logged out');
            this.storage.clear();
            this.app.getRootNav().setRoot('LoginPage');
          }
        }
      ]
    });
    alert.present();
  }

}
