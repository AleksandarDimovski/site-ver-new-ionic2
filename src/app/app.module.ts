import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { ParseError } from "../services/parseError";
import { MyApp } from './app.component';
import { OdooRPCService } from "../services/jsonRpc";
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { HttpModule } from '@angular/http';
import { IonicStorageModule } from '@ionic/storage';
import { AutoPage } from '../components/autocomplete/auto-page/auto-page';

@NgModule({
  declarations: [
    MyApp,
    AutoPage,
    
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot(),
    HttpModule,
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp ,
    AutoPage,
  ],
  providers: [
    StatusBar,
    SplashScreen,
    OdooRPCService,
    ParseError,
    AutoPage,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
