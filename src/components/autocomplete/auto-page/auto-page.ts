import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,ViewController } from 'ionic-angular';
import { OdooRPCService} from "../../../services/jsonRpc";
/**
 * Generated class for the AutoPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-auto-page',
  templateUrl: 'auto-page.html',
   styles: [
        `
          .spinner-container {
        width: 100%;
        text-align: center;
        padding: 10px;
    }
    `
    ]
})
export class AutoPage {
  myInput:any;
  title: string;
  items: any = [];
  searching: boolean = false;

  constructor(public navCtrl: NavController, public viewCtrl: ViewController,public navParams: NavParams,private odooRPC:OdooRPCService) {
    
     let data = navParams.get('data');
     this.title = data.name;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AutoPage');
  }
  onInput(){
     let data = this.navParams.get('data');
  
    if(this.myInput.length >= 3){
      this.searching = true;
       this.odooRPC.call(data.search, 'search_read', [[[data.fields, 'ilike', this.myInput],data.domain]], { 'fields': ['name', 'id','display_name'] }).then(res => {
       
       console.log(res);
       console.log(data.search);
       console.log(data.fields);
       console.log(data.domain); 

       this.searching = false;
       this.items = res;
       
    }, (err => {
          console.log(err);
       this.searching = false;
      
    }))


    }else{
        this.items = [];
    }
   
  }
  onCancel(){
     this.searching = false;
      this.items = [];
     
  }
  itemSelected(item){
    this.viewCtrl.dismiss(item);
  }
   dismiss() {
   let data = null;
   this.viewCtrl.dismiss(data);
 }

}
