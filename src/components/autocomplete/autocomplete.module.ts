import {NgModule } from '@angular/core';
import {IonicModule } from 'ionic-angular';
import {AutoComplete} from './autocomplete';


@NgModule ({
    declarations: [
        AutoComplete,
    ],
    imports:[
        IonicModule,
      
    ],
    exports:[
        AutoComplete
    ]
})
export class AutoCompleteModule  {}