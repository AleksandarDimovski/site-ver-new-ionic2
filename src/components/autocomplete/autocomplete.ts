import { Component, Input, Output, EventEmitter, OnChanges, SimpleChanges } from '@angular/core'
import { FormControl } from '@angular/forms';
import 'rxjs/add/operator/debounceTime';
import { OdooRPCService } from "../../services/jsonRpc";
import { AutoPage } from "./auto-page/auto-page"
import { ModalController, NavParams } from 'ionic-angular';
@Component({
    selector: 'auto-complete',
    providers: [OdooRPCService],
    styles: [
        `input[type="text"][disabled] {
            color: black;
            opacity:1;
}
          .spinner-container {
        width: 100%;
        text-align: center;
        padding: 10px;
    }
    `
    ],
    template: ` 
    
  <ion-item>
  {{autoModel | json}}
    <ion-label color="primary"  stacked >{{title}}</ion-label>
    <ion-input  [disabled]='true' [(ngModel)]="autoModel.display_name"  type="text"></ion-input>
     <button type='button'ion-button color="secondary" [disabled]='disableButton'(click)="openAutoComplete()"  primary item-right>search</button>
  </ion-item>
   
    `
})

export class AutoComplete {
    autoModel: any;
    @Input() disableme: boolean; // the title of the input field 
    @Input() name: string; // the title of the input field 
    @Input() model: any;  // the binding model
    @Output() modelUpdate = new EventEmitter<any>(); //  on change of model emit event
    @Input() search: any;  // the table search
    @Input() fields: any;  // fields to get
    @Input() editValue: any;  // edut Value
    @Input() domain: any;  // edut Value
    title: string = '';  // title of input 
    disableButton: boolean = false;

    constructor(private odooRPC: OdooRPCService, public modalCtrl: ModalController) {
        this.autoModel = {}

    }

    ngOnInit() {


        this.title = this.name;
        if (this.model != undefined || this.model != false) {
            this.autoModel.display_name = this.editValue;
            this.modelUpdate.emit(this.model);
        }

        if (this.disableme) {
            if (this.disableme == true) {
                this.disableButton = true;
            }
        } else {
            this.disableme = false;
        }


    }
    ngOnChanges() {

        if (this.disableme) {
            if (this.disableme == true) {
                this.disableButton = true;
            }
        } else {
            this.disableme = false;
        }


    }



    openAutoComplete() {
        let profileModal = this.modalCtrl.create(AutoPage, { data: { name: this.name, search: this.search, fields: this.fields, domain: this.domain } });
        profileModal.present();
        profileModal.onDidDismiss(data => {

            if (data != null) {
                this.autoModel = data
                this.modelUpdate.emit(data.id);
            }
        });

    }



}


/*import { Directive, ElementRef, Input } from '@angular/core';
@Directive({ selector: '[myHighlight]' })

export class HighlightDirective {

    constructor(el: ElementRef) {
           <ion-searchbar
  [(ngModel)]="searchTerm"
  [showCancelButton]="true"
  (ionInput)="onInput($event)"
  (ionCancel)="onCancel($event)">
</ion-searchbar>
  <div *ngIf="searching" class="spinner-container">
        <ion-spinner></ion-spinner>
    </div>
    <ion-list>
        <ion-item *ngFor="let item of items" (click)="itemSelected(item)">
            {{item.name}}
        </ion-item>
    </ion-list>
       
       el.nativeElement.style.backgroundColor = 'yellow';
    }
      @Input() highlightColor: string;
       ngOnInit(){
     console.log(this.highlightColor)
  }
       
}
*/