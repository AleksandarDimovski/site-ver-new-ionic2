import { NgModule } from '@angular/core';
import { NextPageComponent } from './next-page/next-page';
import { SubmitComponent } from './submit/submit';
import { EquipmentBasicTemplateComponent } from './equipment-basic-template/equipment-basic-template';
import { SurveyComponent } from './survey/survey';

@NgModule({
	declarations: [NextPageComponent,
    SubmitComponent,
    EquipmentBasicTemplateComponent,
    SurveyComponent],
	imports: [],
	exports: [NextPageComponent,
    SubmitComponent,
    EquipmentBasicTemplateComponent,
    SurveyComponent]
})
export class ComponentsModule {}
