import {NgModule } from '@angular/core';
import {IonicModule } from 'ionic-angular';
import {OdooDropDownComponent} from './dropdown';

@NgModule ({
    declarations: [
        OdooDropDownComponent,
    ],
    imports:[
        IonicModule,
    ],
    exports:[
        OdooDropDownComponent
    ]
})
export class OdooDropDownModule  {}