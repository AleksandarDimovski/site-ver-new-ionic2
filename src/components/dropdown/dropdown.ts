import { Component, Input, Output, EventEmitter } from '@angular/core'
import { FormControl } from '@angular/forms';
import 'rxjs/add/operator/debounceTime';
import { OdooRPCService } from "../../services/jsonRpc";
@Component({
  selector: 'odoo-dropdown',
  providers: [OdooRPCService],
  styles: [
    `
      .spinner-container {
        width: 100%;
        text-align: center;
        padding: 10px;
      }
    `
  ],
  template: ` 
    <ion-item>
  <ion-label color="primary"  stacked>{{title}}</ion-label>
  <ion-select   [disabled]='disableField'(ionChange)="newSelection(selectModel)" [(ngModel)]="selectModel">
    <ion-option *ngFor="let item of items" [value]="item.id">{{item.name}}</ion-option>
    </ion-select>
{{selectModel}}
</ion-item>
    <button type='button' (click)='reload()' *ngIf='showRetry' >retry</button>
  
   
    `
})

export class OdooDropDownComponent {
  autoModel: any;
  @Input() disableme: boolean;
  @Input() name: string; // the title of the input field 
  @Input() model: any;  // the binding model
  @Output() modelUpdate = new EventEmitter<any>(); //  on change of model emit event
  @Input() search: any;  // the table search
  @Input() searchFilter: any;
  disableField: boolean = false;
  title: string = '';
  items: any;
  showRetry: boolean = false;
  selectModel: any;
  private doOdooCall(searchFilter) {

    this.odooRPC.call(this.search, 'search_read', searchFilter, { 'fields': ['name', 'id'] }).then(res => {

      this.items = res;
      this.showRetry = false;

    }, (err => {
      this.showRetry = true;
      console.log(err)
    }));

  }

  constructor(private odooRPC: OdooRPCService) {

    if (!this.searchFilter) {
      this.searchFilter = [];
    }





  }
  ngOnChanges() {
    if (this.model) {
      if (this.model != false) {
        this.selectModel = this.model[0] || this.model;
        this.modelUpdate.emit(this.model[0] || this.model);
      }

    }

    if (this.disableme) {
      if (this.disableme == true) {
        this.disableField = true;
      }
    } else {
      this.disableme = false;
    }


  }
  ngOnInit() {
    if (this.model) {
      if (this.model != false) {

        this.selectModel = this.model[0] || this.model;
        this.modelUpdate.emit(this.model[0] || this.model);
      }

    }

    if (this.disableme) {
      if (this.disableme == true) {
        this.disableField = true;
      }
    } else {
      this.disableme = false;
    }


    this.title = this.name;
    this.doOdooCall(this.searchFilter);
  }
  reload() {
    this.doOdooCall(this.searchFilter);
  }
  newSelection(data) {

    this.modelUpdate.emit(data);

  }






}


/*import { Directive, ElementRef, Input } from '@angular/core';
@Directive({ selector: '[myHighlight]' })

export class HighlightDirective {

    constructor(el: ElementRef) {
           <ion-searchbar
  [(ngModel)]="searchTerm"
  [showCancelButton]="true"
  (ionInput)="onInput($event)"
  (ionCancel)="onCancel($event)">
</ion-searchbar>
  <div *ngIf="searching" class="spinner-container">
        <ion-spinner></ion-spinner>
    </div>
    <ion-list>
        <ion-item *ngFor="let item of items" (click)="itemSelected(item)">
            {{item.name}}
        </ion-item>
    </ion-list>
       
       el.nativeElement.style.backgroundColor = 'yellow';
    }
      @Input() highlightColor: string;
       ngOnInit(){
     console.log(this.highlightColor)
  }
       
}
*/