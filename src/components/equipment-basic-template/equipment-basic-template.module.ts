import {NgModule } from '@angular/core';
import {IonicModule } from 'ionic-angular';
import {EquipmentBasicTemplateComponent} from './equipment-basic-template';
import { OdooDropDownModule } from '../../components/dropdown/dropdown.module';
import { AutoCompleteModule } from '../../components/autocomplete/autocomplete.module';
import { SubmitModule } from '../../components/submit/submit.module';

@NgModule ({
    declarations: [
        EquipmentBasicTemplateComponent,
    ],
    imports:[
        IonicModule,
        OdooDropDownModule,
        AutoCompleteModule,
        SubmitModule,
    ],
    exports:[
        EquipmentBasicTemplateComponent
    ]
})
export class  EquipmentBasicTemplateModule  {}