import { Component, Input } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, Events } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { OdooRPCService } from "../../services/jsonRpc";
import { AlertController } from 'ionic-angular';
import { ParseError } from "../../services/parseError";
import * as _ from "underscore";


/**
 * Generated class for the EquipmentBasicTemplateComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'equipment-basic-template',
  templateUrl: 'equipment-basic-template.html'
})
export class EquipmentBasicTemplateComponent {
  equipment:any;
  equip:any;
  siteForm: FormGroup;
  name: string = '';
  id:number;
  @Input() type: string;

  constructor( public navCtrl: NavController, 
    public navParams: NavParams, 
    private storage: Storage,
    public formBuilder: FormBuilder,
    public events: Events,
  ) {

      this.siteForm = this.formBuilder.group({
        id: [''],
        name: [''],
        warranty_end: [''],
        vendor_id: [''],
        asset_id: [''],
        maker_id: [''],
        model: [''],
        serial_num: [''],
        note: [''],

        is_onland: [false],
        has_parrent: [false],
        has_chield:[false],
        coor_top_x: ['', Validators.compose([Validators.pattern('[0-9. ]*'), Validators.required])],
        coor_top_y: ['', Validators.compose([Validators.pattern('[0-9. ]*'), Validators.required])],
        coor_bottom_x: ['', Validators.compose([Validators.pattern('[0-9. ]*'), Validators.required])],
        coor_bottom_y: ['', Validators.compose([Validators.pattern('[0-9. ]*'), Validators.required])],
        coor_margin: ['', Validators.compose([Validators.pattern('[0-9. ]*'), Validators.required])],
        power_ac: ['', Validators.compose([Validators.pattern('[0-9. ]*'), Validators.required])],
        power_dc: ['', Validators.compose([Validators.pattern('[0-9. ]*'), Validators.required])],
        
      });
      
      this.storage.get('Equipment').then((equipment) => {
        let equip = _.where(equipment, {type: this.type} );
        this.equip = equip;
        console.log(this.equip);
        for (let i in this.equip) {
          this.name = equip[i].name ;
          this.id = equip[i].id ;
          this.siteForm.patchValue({
    
                    id: equip[i].id,
                    name: equip[i].name,
                    vendor_id: equip[i].vendor_id,
                    asset_id: equip[i].asset_id,
                    warranty_end: equip[i].warranty_end,
                    maker_id: equip[i].maker_id,
                    model: equip[i].model,
                    serial_num: equip[i].serial_num,
                    note: equip[i].note,

                    is_onland: equip[i].is_onland,
                    has_parrent: equip[i].has_parrent,
                    has_chield: equip[i].has_chield,
                    coor_top_x: equip[i].coor_top_x,
                    coor_top_y: equip[i].coor_top_y,
                    coor_bottom_x: equip[i].coor_bottom_x,
                    coor_bottom_y: equip[i].coor_bottom_y,
                    coor_margin: equip[i].coor_margin,
                    power_ac: equip[i].power_ac,
                    power_dc: equip[i].power_dc
      

 
     }); 
     console.log('ID',equip[i].id)
     }
     });
  }

}
