import {NgModule } from '@angular/core';
import {IonicModule } from 'ionic-angular';
import {NextPageComponent} from './next-page';

@NgModule ({
    declarations: [
        NextPageComponent,
    ],
    imports:[
        IonicModule,
    ],
    exports:[
        NextPageComponent
    ]
})
export class NextPageModule  {}