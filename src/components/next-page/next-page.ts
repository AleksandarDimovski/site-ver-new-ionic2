import { Component, Input } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the NextPageComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'next-page',
  templateUrl: 'next-page.html'
})
export class NextPageComponent {
  @Input() name: string;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    
  }
  clicked(event){
    this.navCtrl.push(this.name)

  }

}
