import {NgModule } from '@angular/core';
import {IonicModule } from 'ionic-angular';
import {SubmitComponent} from './submit';

@NgModule ({
    declarations: [
        SubmitComponent,
    ],
    imports:[
        IonicModule,
    ],
    exports:[
        SubmitComponent
    ]
})
export class SubmitModule  {}