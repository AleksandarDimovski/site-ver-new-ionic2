import { Component } from '@angular/core';

/**
 * Generated class for the SubmitComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'submit',
  templateUrl: 'submit.html'
})
export class SubmitComponent {

  text: string;

  constructor() {
    console.log('Hello SubmitComponent Component');
    this.text = 'Hello World';
  }

}
