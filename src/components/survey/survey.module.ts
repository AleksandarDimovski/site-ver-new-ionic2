import {NgModule } from '@angular/core';
import {IonicModule } from 'ionic-angular';
import {SurveyComponent} from './survey';

@NgModule ({
    declarations: [
        SurveyComponent,
    ],
    imports:[
        IonicModule,
    ],
    exports:[
        SurveyComponent
    ]
})
export class SurveyModule  {}