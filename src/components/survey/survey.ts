import { Component, Input } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, Events, ModalController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { FormGroup, FormBuilder } from '@angular/forms';
import { OdooRPCService } from "../../services/jsonRpc";
import { AlertController, LoadingController } from 'ionic-angular';
import { ParseError } from "../../services/parseError";
import * as _ from "underscore";
import * as $ from 'jquery'
/**
 * Generated class for the SurveyComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'survey',
  templateUrl: 'survey.html'
})
export class SurveyComponent {
  public hideShowSurvey: boolean = false;
  surv:any;
  rowCollection = [];
  aditional:any;
  modal:any;
  dd:any;
  name: string = '';
  drhddName: string = '';
  drhd_number: number;
  drhd_date:any;
  drhd_detail: string = '';
  @Input() step: any;
  @Input() miltiply: string;
  @Input() eqId: number;


  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams, 
    private storage: Storage,
    private odooRPC: OdooRPCService, 
    public formBuilder: FormBuilder,
    private alertCtrl: AlertController,
    private toastCtrl: ToastController,
    public events: Events,
    private parse: ParseError,
    public modalCtrl: ModalController,
  ) {

    this.storage.get('Survey').then((survey) => {
      console.log(survey);
      //let surv = _.where(survey, {drtg_mobapp_step: this.step} );
      //this.surv = surv;
      //console.log(this.surv);

      let questions = [];
      if(this.miltiply == "no") {
        for(let o in survey) {
          if(survey[o].drtg_mobapp_step == this.step){
            questions.push(survey[o]);
            this.name = survey[o].name;
            this.drhddName = survey[o].drhddName;
            this.drhd_number = survey[o].drhd_number;
            this.drhd_date = survey[o].drhd_date;
            this.drhd_detail = survey[o].drhd_detail;
          }

        }
        
        this.rowCollection = questions;
     
        console.log('name', this.rowCollection[0].name)
      } else {
        for(let i in survey) {
          if(survey[i].drtg_mobapp_step == this.step && survey[i].drtg_multi_no == 1) {
            questions.push(_.clone(survey[i]));
            this.name = survey[i].name;
            this.drhddName = survey[i].drhddName;
            this.drhd_number = survey[i].drhd_number;
            this.drhd_date = survey[i].drhd_date;
            this.drhd_detail = survey[i].drhd_detail;
          }
        }
        this.rowCollection = questions;
        console.log(this.rowCollection);

        for(let z in survey) {
          if(survey[z].drtg_mobapp_step == this.step && survey[z].drtg_multi_no != 1 && survey[z].equipment_id[0] == this.eqId){
            for (let p in this.rowCollection) {
              if(this.rowCollection[p].drtd_id[0] == survey[z].drtd_id[0]) {
                this.rowCollection[p] = survey[z]
                this.name = survey[z].name;
                this.drhddName = survey[z].drhddName;
                this.drhd_number = survey[z].drhd_number;
                this.drhd_date = survey[z].drhd_date;
                this.drhd_detail = survey[z].drhd_detail;

                console.log(this.rowCollection[p]);
              }
            }

          }
        }

      }

    });
   
  }
  itemClicked(data, index) {
    this.aditional = [["drtd_id", "=", data.drtd_id[0]]];
      let modal = this.modalCtrl.create(data, {});
      let coopy = _.clone(data);
      coopy.index = index;
      this.dd = coopy; 
      modal.present();
  

  }
create(model){
    let object = {
      drhd_date: model.drhd_date,
      drhd_detail: model.drhd_detail,
      drhd_number: model.drhd_number,
      drhdd_id: model.drhdd_id,
      equipment_id: this.eqId,
      drt_id: model.drt_id[0],
      drtd_id: model.drtd_id[0],
      drtg: model.drtg,
      drh_id: model.drh_id[0],
      name: model.name,
      drtg_id: model.drtg_id,
      drtg_multi_no: 4
    };

    this.odooRPC.call('tower.site.survey.datarec.history.dtlnew', 'create', [object], {}).then(data => {
      let selected = $(".mySelect option:selected").text();

      this.rowCollection[model.index].drhd_date = model.drhd_date;
      this.rowCollection[model.index].drhd_detail = model.drhd_detail;
      this.rowCollection[model.index].drhd_number = model.drhd_number;
      this.rowCollection[model.index].drhdd_id = model.drhdd_id;
      this.rowCollection[model.index].drhddName = selected;
      this.rowCollection[model.index].equipment_id = this.eqId;
      this.rowCollection[model.index].id = data;

      this.modal.dismiss();

    }).catch(err => {
      // alert some kind of errpr
       let alert = this.alertCtrl.create({
        title: 'Failed To Sumbit',
        subTitle: 'Please Try Again: ' + (err.message || this.parse.getError(err)),
        buttons: ['Dismiss']
      });
      alert.present();
    });

}
submit (model) {
  let object = {
    drhd_date: model.drhd_date,
    drhd_detail: model.drhd_detail,
    drhd_number: model.drhd_number,
    drhdd_id: model.drhdd_id,
    equipment_id: this.eqId
  };
  this.odooRPC.call('tower.site.survey.datarec.history.dtlnew', 'write', [[model.id],
  object], {}).then(data => {
    let selected = $(".mySelect option:selected").text();
    if (data == true) {
      for (var i in  this.rowCollection) {
        if (
          this.rowCollection[i].id == model.id) {
          this.rowCollection[i].drhd_date = model.drhd_date;
          this.rowCollection[i].drhd_detail = model.drhd_detail;
          this.rowCollection[i].drhd_number = model.drhd_number;
          this.rowCollection[i].drhdd_id = model.drhdd_id;
          this.rowCollection[i].drhddName = selected;
        }
      }
      this.modal.remove();
    }

   
  }).catch(err => {
    // alert some kind of errpr
     let alert = this.alertCtrl.create({
      title: 'Failed To Sumbit',
      subTitle: 'Please Try Again: ' + (err.message || this.parse.getError(err)),
      buttons: ['Dismiss']
    });
    alert.present();
  });
}
public hideShowSurveyButton() {
  this.hideShowSurvey = !this.hideShowSurvey;
}

}
