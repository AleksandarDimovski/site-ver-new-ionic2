import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform } from 'ionic-angular';
import { Validators, FormGroup, FormBuilder } from '@angular/forms';
import { OdooRPCService } from "../../services/jsonRpc";
import { AlertController, LoadingController } from 'ionic-angular';
import { Events } from 'ionic-angular';
import { Http } from '@angular/http';

/**
 * Generated class for the LoginPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
loginForm: FormGroup;
  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    private odooRPC: OdooRPCService, 
    public formBuilder: FormBuilder, 
    public events: Events,
    public loadingCtrl: LoadingController, 
    private alertCtrl: AlertController,
    public platform: Platform,
    public http: Http,
  ) {

    //this.storage.clear();
    
   
   
    this.loginForm = this.formBuilder.group({
      username: ['admin', Validators.required],
      password: ['admin', Validators.required],
    });
     
   }
  ionViewDidLoad() {

    
  }

  loginMe(data: any): void {
    this.loginUser(data.value.username, data.value.password)
  }

  private loginUser(username, password) {
    let loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    loading.present();

    this.odooRPC.login('ts_test_003', username, password).then(res => {
       
      this.odooRPC.call('hr.employee', 'search_read', [[['user_id','=',res.uid]]], { 'fields': ["user_id",
      'id', 'login','display_name'] }).then(resUser => {
        let user = {
          username: username,
          session_id: res.session_id,
          password: password,
          display_name: resUser[0].display_name
        }
        loading.dismiss();
        this.events.publish('user-is-logged', user);
        this.navCtrl.setRoot('Step1Page');
      }).catch(err => {
        console.log(err)
        loading.dismiss();
        let alert = this.alertCtrl.create({
          title: 'Login failed - try again',
          subTitle: (err.message || err),
          buttons: ['Dismiss']
        });
        alert.present();
      })
    }).catch(err => {
  
     loading.dismiss();
      let alert = this.alertCtrl.create({
        title: 'Login failed - try again',
        subTitle: (err.message || err),
        buttons: ['Dismiss']
      });
      alert.present();
    })
  }
}

