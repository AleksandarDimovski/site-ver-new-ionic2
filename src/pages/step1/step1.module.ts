import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Step1Page } from './step1';
import { NextPageModule } from '../../components/next-page/next-page.module';
@NgModule({
  declarations: [
    Step1Page,
  ],
  imports: [
    IonicPageModule.forChild(Step1Page),
    NextPageModule,
  ]
})
export class Step1PageModule {}
