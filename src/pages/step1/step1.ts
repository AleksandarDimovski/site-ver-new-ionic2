import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AlertController, ToastController, LoadingController } from 'ionic-angular';
import { OdooRPCService } from "../../services/jsonRpc";
import { Storage } from '@ionic/storage';
import  _  from 'underscore';

/**
 * Generated class for the Step1Page page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-step1',
  templateUrl: 'step1.html',
})
export class Step1Page {
  found = 0;
  noresult: boolean = true; 
  myInput: any;
  searching: boolean = false;
  spinner: boolean = true;
  sites: any = [];
  survey: any = [];
  site_id: any;
  dataHistoryId: any;
  equipments: any;
  basicData:any;
  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams, 
    private odooRPC: OdooRPCService, 
    private alertCtrl: AlertController, 
    public toastCtrl: ToastController,
    private storage: Storage,
    public loadingCtrl: LoadingController,
  ) {
  
  
  }
 searchSites(myInput:any){

  let loading = this.loadingCtrl.create({
    content: 'Please wait...'
  });
  loading.present();

    this.noresult = true;
  
     if(this.myInput.length > 3){
      this.searching = true;
      this.spinner = false;
      this.odooRPC.call('telco.tower.site', 'search_read', [[['ts_site_id', 'ilike', this.myInput],["ver_flag", "=", 1], ["ver_status", "=", "Draft"]]], 
      {'fields': ['name', 'id', "ts_alias_id", "site_rental_ids"]}).then(result => {
      if(result.length > 0) { 
        let siteID = {id: ''}
        this.searching = false;
        this.spinner = true; 
        this.sites = result;
        siteID.id = result[0].id;
          this.odooRPC.call('telco.tower.site.survey', 'search_read', [[['tower_site_id', '=', siteID.id],["state", "=", "inprogress"]]], {
            "fields": ["state", "tower_site_id", "drtg_mobapp_step", "id", "tss_datarec_history_id", "tss_pictures_ids"] }).then(re => {
            let surveyID = {id: ''}
            let dataHistoryId = {id: ''}
            this.survey = re;
            if (re.length == 0) {
              let alert = this.alertCtrl.create({
                title: 'Survey not found',
                subTitle: 'Please try again',
                buttons: ['Dismiss']
              });
              alert.present();
            }
            surveyID.id = re[0].id;
            dataHistoryId = re[0].tss_datarec_history_id[0];
            this.odooRPC.call('tower.site.survey.datarec.history.dtlnew', 'search_read', [[["drh_id", "=", dataHistoryId]]], {}).then(r => {
  
              let array = [];
  
              for (let i in r) {
                if (r[i].drhd_date == false) {
                  r[i].drhd_date = null;
                }
                if (r[i].drhd_detail == false) {
                  r[i].drhd_detail = '';
                }
                if (r[i].drhd_number == 0) {
                  r[i].drhd_number = '';
                }
                if (r[i].drhdd_id == false) {
                  r[i].drhdd_id = '';
                }
  
               array.push({
                  equipment_id: r[i].equipment_id,
                  drtg_mobapp_step: r[i].drtg_mobapp_step,
                  drtg: r[i].drtg_id[1],
                  id: r[i].id,
                  drhd_status: r[i].drhd_status,
                  drhd_number: r[i].drhd_number,
                  drhd_detail: r[i].drhd_detail,
                  drhd_date: r[i].drhd_date,
                  drtd_id: r[i].drtd_id,
                  drhddName: r[i].drhdd_id[1],
                  drhdd_id: r[i].drhdd_id[0],
                  drt_id: r[i].drt_id,
                  site_rental_id: r[i].site_rental_id,
                  site_rental_eqdtl_id: r[i].site_rental_eqdtl_id,
                  drh_id: r[i].drh_id,
                  name: r[i].name,
                  drtg_id: r[i].drtg_id[0],
                  drtg_multi_no: r[i].drtg_multi_no,
                  b_number:r[i].b_number,
                  b_date:r[i].b_date,
                  b_detail:r[i].b_detail,
                  b_dropdown:r[i].b_dropdown
                });
              }
  
  
              this.storage.set('Survey', array).then(() => {
    
              });
              
              this.odooRPC.call('telco.tower.site', 'search_read', [[["id", "=", siteID.id]]], {'fields': ['site_surroundings_id', 'site_type_id', 'site_project_group_id', "tower_type_id",
              "tower_supplier", "tower_model", "tower_design_code", "to_towerheight", "to_height_building",
              "dcalm2", "design_antena_area", "tower_dws","note",
              "tower_tt_certificate", "tower_certificate",
              "tower_grounding_provided", "full_arrest_system", "tower_galvanization", "tower_steel_test_report", "tower_paint",
              "tower_material_spec", "et_inoutdoor", "et_winm", "et_linm", "et_hinm", "et_design_area", "et_sunshade", "cp_threephase_avail", "cp_electrical_company", "cp_wapda", "cp_status",
              "cp_grid_station_loc", "cp_availab", "cp_con_meter", "cp_energy_meter_type", "cp_application", "cp_transform_install", "cp_trnsform_size",
              "cp_trnsform_rating", "cp_trnsform_sl_kva", "cp_trnsform_sl_kw", "cp_avail_vs", "cp_type_conn", "cp_tariff_id", "cp_vol_l1", "cp_vol_l2",
              "cp_vol_l3", "cp_vol_ng", "cp_annave_opd", "cp_avg_mb", "sap_status", "sap_ogh", "sap_seccompany_id", "sap_seccompany_contact", "sap_seccompany_gdc",
              "sap_seccompany_gnc", "sap_bwallc", "sap_gate_install", "sap_fence_install", "sap_details", "sap_independent_access", "sap_24x7access", "sap_bw_constructed",
              "site_lease_id", "cp_tariff", "site_constr_date", "cp_conn_date", "cp_application_date", "cp_approved_date"]}).then(results => {
                console.log('results', results);
                this.basicData = results[0];
              let equipmentIds = [];
              this.storage.set('BasicData', this.basicData);
          
  
              this.odooRPC.call('telco.site.equipment', 'search_read', [[["tower_site_id", "=", siteID.id]]], {}).then(result => {
                console.log('result', result);
    
                for (let i in result) {
                  result[i].equipment_id = result[i].equipment_id[0];
                
                  equipmentIds.push(result[i].equipment_id);
                }
  
                this.odooRPC.call('telco.equipment', 'read', [equipmentIds], {'fields': [ "id",
                "cp_grid_station_loc","shelter_cover_cable","cp_colourcab_n","tower_material_spec","gs_ats","atsamf_dcbc","ipsats_inscost","gs_cmodel",
                "cp_colourcab_b","batt_inout","cp_colourcab_y","rms_fa_ips","rms_insby","gs_fuel_tank_l","atsamf_housing","gs_ats_panel","solar_typecells",
                "cp_colourcab_r","tank_shape","batt_fobcost","ipsats_acdb_details","ipsats_built_aps","batt_cstatus","tower_dws","atsamf_ip_rating",
                "shelter_cut_power","ipsats_line_cond","ipsats_insby","tower_paint","batt_inscost","atsamf_type","db_antenna","rms_inscost","tower_tt_certificate",
                "vendor_id","gs_alarm","batt_conntenant","gs_inroom","rec_nomodules","mhz_antenna","gs_cleaning","name","rec_powerrac","an_pole","ipsats_dim_cabinet",
                "ipsats_sys_monitor","cp_wapda","cp_spare_breakers","cp_con_meter","cp_electrical_company","linecon_out_v","tower_type_id","an_braced_pole",
                "design_antena_area","cp_conn_powfeeders","rec_ratingmodules","cp_payment","asset_id","cp_trnsform_rating","cp_reference","shelter_type_id",
                "shelter_door","gs_hmr","cp_trnsform_install","solar_narrays","dc_power","cp_dlink","ipsats_housing","tower_galvanization","gs_kva_actual",
                "shelter_lock","gs_acum__rh","gs_canopy_ins","cp_path_powfeeders","ipsats_gbsc","linecon_housing","tank_fs_installed","tank_dim_cylindric","gs_paint",
                "cp_present_meter_reading","tower_stability","gs_panel_sn","rms_modules","ipsats_amf_ats","rms_la_rmsnoc","to_towerheight","ipsats_dctlc","batt_ucap","linecon_type",
                "batt_v","cp_trnsform_ground","ipsats_rating","linecon_rating","ipsats_built_lc","shelter_fire_cbox","height","gs_fonload", "rec_mmcost",
                "cp_ac_db_availab","gs_fuel_avgcons","cp_dn_payment","create_uid","cp_threephase","message_last_post","linecon_inp_v","gs_ats_breaker",
                "tower_grounding_provided","cp_trnsform_sl_kva","atsamf_pmm","type","type_r","gs_engine_sn","ipsats_uovp","gs_fuel_tank","cp_trnsform_indep",
                "cp_po","shelter_plot_size","cp_powcable_len","gs_ats_cabling","cp_dwapda_con","rec_fobcost","rms_mmcost","solar_dim","shelter_room_dim","shelter_wleader","cp_trnsform_sl_kw",
                "tower_agwl_qty","gs_fuel_mc","rms_insstatus","atsamf_hw","dia_mw_dash","full_arrest_system","gs_canoy","shelter_earthing_cable","tower_steel_test_report",
                "shelter_room","rec_conntenant","shelter_room_num","batt_housing","dcalm2","ipsats_type","shelter_room_body","note","gs_kva_rating","shelter_siren_test",
                "tank_dim_cubical","shelter_check_light","batt_mmcost","ipsats_mmcost","maker_id","gs_montly_rh","ipsats_en_metering","write_uid","tower_Tightening",
                "shelter_check_fire_cbox","an_guyed_pole","ipsats_spd","tower_foundation","tank_capacity","shelter_cable_tray","cp_appear_powfeeders","ipsats_ip_rating",
                "tower_design_code","cp_availab","rec_inscost","serial_num","solar_capacity","model","tower_site_idr","batt_rcap","rms_la_mnonoc",
                "power_ac","coor_top_x","coor_top_y","coor_bottom_y","coor_margin","power_dc","is_onland","coor_botoom_x","coor_bottom_x","techroom_type",
                "pads_type","id_ver","warranty_end","gs_installdate","gs_lastoilcange","gs_lastprh","tank_installdate","rec_date","batt_date","solar_ins_date"
              ]}).then(equipment => {
                console.log('equipment', equipment);
                  for(let i in equipment){
                    equipment[i].real_equipment_id = equipment[i].id;
                  }
                  
                  let equip = _.map(equipment, function (item) {
                    return _.extend(item, _.findWhere(result, {equipment_id: item }));
                   
                  });
                  console.log(equip);
                  this.storage.set('Equipment', equip);
                  loading.dismiss();
                  this.found = 1;
                  
              },(err => {
                loading.dismiss();
                let toast = this.toastCtrl.create({
                  message: 'Failed reading basic data for equipment',
                  duration: 1000,
                  position: 'bottom'
                });
                toast.present();
              }))  
              loading.dismiss();
              },(err => {
                loading.dismiss();
                let toast = this.toastCtrl.create({
                  message: 'Failed loading equipment',
                  duration: 1000,
                  position: 'bottom'
                });
                toast.present();
          
          
              }))  
  
              loading.dismiss(); 
  
              },(err => {
                loading.dismiss();
                let toast = this.toastCtrl.create({
                  message: 'failded',
                  duration: 1000,
                  position: 'bottom'
                });
                toast.present();
          
          
              }))  
  
              loading.dismiss();
  
            },(err => {
              loading.dismiss();
              let toast = this.toastCtrl.create({
                message: 'failded',
                duration: 1000,
                position: 'bottom'
              });
              toast.present();
        
        
            })) 
  
  
            loading.dismiss();
  
          }, (err => {
            loading.dismiss();
            let toast = this.toastCtrl.create({
              message: 'Survey failded',
              duration: 1000,
              position: 'bottom'
            });
            toast.present();
      
      
          })) 
        if (result.length == 0) {
          this.noresult = false;
          let alert = this.alertCtrl.create({
            title: 'Site not found',
            subTitle: 'Please try again with different site name',
            buttons: ['Dismiss']
          });
          alert.present();
        }
        loading.dismiss();
      } else {
        let alert = this.alertCtrl.create({
          title: 'Site not found',
          subTitle: 'Site Des Not Exist, Or It May Not Have A Verification Record',
          buttons: ['Dismiss']
        });
        alert.present();
        loading.dismiss();
        this.found = 0;
      }
    }, (err => {
      loading.dismiss();
      this.searching = false;
      this.spinner = true; 
      let toast = this.toastCtrl.create({
        message: 'Search failed!',
        duration: 1000,
        position: 'bottom'
      });
      toast.present();


    }))
    } else {
      this.sites = [];
      loading.dismiss();

    }

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Step1Page');
  }

}
