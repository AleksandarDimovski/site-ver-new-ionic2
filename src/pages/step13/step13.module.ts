import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Step13Page } from './step13';
import { NextPageModule } from '../../components/next-page/next-page.module';
import { OdooDropDownModule } from '../../components/dropdown/dropdown.module';
import { AutoCompleteModule } from '../../components/autocomplete/autocomplete.module';
import { SubmitModule } from '../../components/submit/submit.module';
import { EquipmentBasicTemplateModule } from '../../components/equipment-basic-template/equipment-basic-template.module';
import { SurveyModule } from '../../components/survey/survey.module';

@NgModule({
  declarations: [
    Step13Page,
  ],
  imports: [
    IonicPageModule.forChild(Step13Page),
    NextPageModule,
    OdooDropDownModule,
    AutoCompleteModule,
    SubmitModule,
    EquipmentBasicTemplateModule,
    SurveyModule
  ],
})
export class Step13PageModule {}
