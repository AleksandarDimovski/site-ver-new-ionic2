import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, Events } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { OdooRPCService } from "../../services/jsonRpc";
import { AlertController } from 'ionic-angular';
import { ParseError } from "../../services/parseError";
import _  from 'underscore';
/**
 * Generated class for the Step19Page page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-step19',
  templateUrl: 'step19.html',
})
export class Step19Page {
  public hideShowContent: boolean = false;
  public hideShowSubContent: boolean = false;
  equipment:any;
  equip:any;
  siteForm: FormGroup;
  name: string = '';

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams, 
    private storage: Storage,
    private odooRPC: OdooRPCService, 
    public formBuilder: FormBuilder,
    private alertCtrl: AlertController,
    private toastCtrl: ToastController,
    public events: Events,
    private parse: ParseError,
  ) {
    
    this.siteForm = this.formBuilder.group({
      solar_ins_date: [''],
      solar_typecells: [''],
      solar_capacity: ['', Validators.compose([Validators.pattern('[0-9. ]*'), Validators.required])],
      solar_dim: [''],
      solar_narrays: ['', Validators.compose([Validators.pattern('[0-9. ]*'), Validators.required])],
    });
    
    this.storage.get('Equipment').then((equipment) => {
      let equip = _.where(equipment, {type: 'solar'} );
      this.equip = equip;
      console.log(this.equip);
      
      for (let i in this.equip) {
        this.name = equip[i].name ;
        this.siteForm.patchValue({
           real_equipment_id: equip[i].real_equipment_id,
           solar_ins_date: equip[i].solar_ins_date,
           solar_typecells: equip[i].solar_typecells,
           solar_capacity: equip[i].solar_capacity,
           solar_dim: equip[i].solar_dim,
           solar_narrays: equip[i].solar_narrays
        });
    }
    
    });
    this.storage.get('Survey').then((survey) => {
      console.log(survey)
    });
  }
  submit(siteForm) {
    console.log(this.equipment[0].id);
    this.odooRPC.call('telco.equipment', 'write', [[this.equipment[0].id], siteForm], {}).then(res => {
      let toast = this.toastCtrl.create({
        message: 'Update sucess',
        duration: 3000,
        position: 'bottom'
      });
      toast.present();
      // toster all is ok and update the parrent with an event
    }).catch(err => {
      // alert some kind of errpr
       let alert = this.alertCtrl.create({
        title: 'Failed To Sumbit',
        subTitle: 'Please Try Again: ' + (err.message || this.parse.getError(err)),
        buttons: ['Dismiss']
      });
      alert.present();
    });
  }
  public hideShowContentButton() {
    this.hideShowContent = !this.hideShowContent;
}
public hideShowContentSubButton() {
  this.hideShowSubContent = !this.hideShowSubContent;
}
  ionViewDidLoad() {
  }

}