import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Step2Page } from './step2';
import { NextPageModule } from '../../components/next-page/next-page.module';
import { OdooDropDownModule } from '../../components/dropdown/dropdown.module';
import { SubmitModule } from '../../components/submit/submit.module';
import { SurveyModule } from '../../components/survey/survey.module';

@NgModule({
  declarations: [
    Step2Page,
  ],
  imports: [
    IonicPageModule.forChild(Step2Page),
    NextPageModule,
    OdooDropDownModule,
    SubmitModule,
    SurveyModule
  ],
})
export class Step2PageModule {}
