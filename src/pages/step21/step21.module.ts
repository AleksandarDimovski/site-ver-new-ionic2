import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Step21Page } from './step21';
import { NextPageModule } from '../../components/next-page/next-page.module';
import { OdooDropDownModule } from '../../components/dropdown/dropdown.module';
import { AutoCompleteModule } from '../../components/autocomplete/autocomplete.module';
import { SubmitModule } from '../../components/submit/submit.module';
import { EquipmentBasicTemplateModule } from '../../components/equipment-basic-template/equipment-basic-template.module';

@NgModule({
  declarations: [
    Step21Page,
  ],
  imports: [
    IonicPageModule.forChild(Step21Page),
    NextPageModule,
    OdooDropDownModule,
    AutoCompleteModule,
    SubmitModule,
    EquipmentBasicTemplateModule,
  ],
})
export class Step21PageModule {}
