import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Step22Page } from './step22';
import { NextPageModule } from '../../components/next-page/next-page.module';
import { OdooDropDownModule } from '../../components/dropdown/dropdown.module';
import { AutoCompleteModule } from '../../components/autocomplete/autocomplete.module';
import { SubmitModule } from '../../components/submit/submit.module';
import { EquipmentBasicTemplateModule } from '../../components/equipment-basic-template/equipment-basic-template.module';

@NgModule({
  declarations: [
    Step22Page,
  ],
  imports: [
    IonicPageModule.forChild(Step22Page),
    NextPageModule,
    OdooDropDownModule,
    AutoCompleteModule,
    SubmitModule,
    EquipmentBasicTemplateModule,
  ],
})
export class Step22PageModule {}
