import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Step23Page } from './step23';
import { NextPageModule } from '../../components/next-page/next-page.module';
import { OdooDropDownModule } from '../../components/dropdown/dropdown.module';
import { AutoCompleteModule } from '../../components/autocomplete/autocomplete.module';
import { SubmitModule } from '../../components/submit/submit.module';
import { EquipmentBasicTemplateModule } from '../../components/equipment-basic-template/equipment-basic-template.module';

@NgModule({
  declarations: [
    Step23Page,
  ],
  imports: [
    IonicPageModule.forChild(Step23Page),
    NextPageModule,
    OdooDropDownModule,
    AutoCompleteModule,
    SubmitModule,
    EquipmentBasicTemplateModule,
  ],
})
export class Step23PageModule {}
