import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, Events } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { OdooRPCService } from "../../services/jsonRpc";
import { AlertController } from 'ionic-angular';
import { ParseError } from "../../services/parseError";

/**
 * Generated class for the Step3Page page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-step3',
  templateUrl: 'step3.html',
})
export class Step3Page {
  basicdata:any;
  siteForm: FormGroup;
  loc: any = [];

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams, 
    private storage: Storage,
    private odooRPC: OdooRPCService, 
    public formBuilder: FormBuilder,
    private alertCtrl: AlertController,
    private toastCtrl: ToastController,
    public events: Events,
    private parse: ParseError,
  ) {
    
    this.siteForm = this.formBuilder.group({
      tower_type_id: [''],
      tower_supplier: [''],
      tower_model: [''],
      tower_design_code: [''],
      to_towerheight: ['', Validators.compose([Validators.pattern('[0-9. ]*'), Validators.required])],
      to_height_building: ['', Validators.compose([Validators.pattern('[0-9. ]*'), Validators.required])],
      dcalm2:['', Validators.compose([Validators.pattern('[0-9. ]*'), Validators.required])],
      design_antena_area: ['', Validators.compose([Validators.pattern('[0-9. ]*'), Validators.required])],
      tower_dws: ['', Validators.compose([Validators.pattern('[0-9. ]*'), Validators.required])],
      tower_certificate: [false],
      full_arrest_system: [false],
      tower_grounding_provided: [false],
      tower_galvanization: [false],
      tower_steel_test_report: [false],
      tower_tt_certificate: [false],
      tower_paint: [false],
      tower_material_spec: ['']

     
    });

    this.storage.get('BasicData').then((basicdata) => {
      
    this.basicdata = basicdata;

    this.siteForm.patchValue({
    tower_type_id:basicdata.tower_type_id,
    tower_supplier:basicdata.tower_supplier,
    tower_model:basicdata.tower_model,
    tower_design_code:basicdata.tower_design_code,
    to_towerheight:basicdata.to_towerheight,
    to_height_building:basicdata.to_height_building,
    dcalm2:basicdata.dcalm2,
    design_antena_area:basicdata.design_antena_area,
    tower_dws:basicdata.tower_dws,
    tower_material_spec:basicdata.tower_material_spec,
    tower_certificate:basicdata.tower_certificate,
    full_arrest_system:basicdata.full_arrest_system,
    tower_grounding_provided:basicdata.tower_grounding_provided,
    tower_galvanization:basicdata.tower_galvanization,
    tower_steel_test_report:basicdata.tower_steel_test_report,
    tower_tt_certificate:basicdata.tower_tt_certificate,
    tower_paint:basicdata.tower_paint,

    });

    });

    this.storage.get('Survey').then((survey) => {
    });
   
  }
  submit(siteForm) {
    this.odooRPC.call('telco.tower.site', 'write', [[this.basicdata.id], siteForm], {}).then(res => {
      let toast = this.toastCtrl.create({
        message: 'Update sucess',
        duration: 3000,
        position: 'bottom'
      });
      
      toast.present();
      // toster all is ok and update the parrent with an event
    }).catch(err => {
      // alert some kind of errpr
       let alert = this.alertCtrl.create({
        title: 'Failed To Sumbit',
        subTitle: 'Please Try Again: ' + (err.message || this.parse.getError(err)),
        buttons: ['Dismiss']
      });
      alert.present();
    });
  }

 
  ionViewDidLoad() {
  }

}
       