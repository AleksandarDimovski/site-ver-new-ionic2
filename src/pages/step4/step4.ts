import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, Events } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { OdooRPCService } from "../../services/jsonRpc";
import { AlertController, LoadingController } from 'ionic-angular';
import { ParseError } from "../../services/parseError";

/**
 * Generated class for the Step4Page page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-step4',
  templateUrl: 'step4.html',
})
export class Step4Page {
  basicdata:any;
  siteForm: FormGroup;
  loc: any = [];

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams, 
    private storage: Storage,
    private odooRPC: OdooRPCService, 
    public formBuilder: FormBuilder,
    private alertCtrl: AlertController,
    private toastCtrl: ToastController,
    public events: Events,
    private parse: ParseError,
  ) {
    
    this.siteForm = this.formBuilder.group({
      et_inoutdoor: [''],
      et_winm: ['', Validators.compose([Validators.pattern('[0-9. ]*'), Validators.required])],
      et_linm: ['', Validators.compose([Validators.pattern('[0-9. ]*'), Validators.required])],
      et_hinm: ['', Validators.compose([Validators.pattern('[0-9. ]*'), Validators.required])],
      et_desing_area: ['', Validators.compose([Validators.pattern('[0-9. ]*'), Validators.required])],
      et_sunshade: [false]
    });

    this.storage.get('BasicData').then((basicdata) => {

      this.basicdata = basicdata;
      this.siteForm.patchValue({
        et_inoutdoor:basicdata.et_inoutdoor,
        et_winm:basicdata.et_winm,
        et_linm:basicdata.et_linm,
        et_hinm:basicdata.et_hinm,
        et_desing_area:basicdata.et_desing_area,
        et_sunshade:basicdata.et_sunshade,
      });
    });
    this.storage.get('Survey').then((survey) => {
    });
  }
  submit(siteForm) {
    this.odooRPC.call('telco.tower.site', 'write', [[this.basicdata.id], siteForm], {}).then(res => {
      let toast = this.toastCtrl.create({
        message: 'Update sucess',
        duration: 3000,
        position: 'bottom'
      });
      toast.present();
      // toster all is ok and update the parrent with an event
    }).catch(err => {
      // alert some kind of errpr
       let alert = this.alertCtrl.create({
        title: 'Failed To Sumbit',
        subTitle: 'Please Try Again: ' + (err.message || this.parse.getError(err)),
        buttons: ['Dismiss']
      });
      alert.present();
    });
  }
  ionViewDidLoad() {
  }

}
