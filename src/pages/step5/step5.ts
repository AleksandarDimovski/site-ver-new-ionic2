import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, Events } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { Validators, FormGroup, FormBuilder } from '@angular/forms';
import { OdooRPCService } from "../../services/jsonRpc";
import { AlertController, LoadingController } from 'ionic-angular';
import { ParseError } from "../../services/parseError";

/**
 * Generated class for the Step5Page page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-step5',
  templateUrl: 'step5.html',
})
export class Step5Page {
  basicdata:any;
  siteForm: FormGroup;
  loc: any = [];

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams, 
    private storage: Storage,
    private odooRPC: OdooRPCService, 
    public formBuilder: FormBuilder,
    private alertCtrl: AlertController,
    private toastCtrl: ToastController,
    public events: Events,
    private parse: ParseError,
  ) {
    
    this.siteForm = this.formBuilder.group({
      cp_status: [''],
      cp_electrical_company: [''],
      cp_wapda: [''],
      cp_grid_station_loc: [''],
      cp_availab: [false],
      cp_con_meter: [false],
      cp_energy_meter_type: [''],
      cp_application_date: [''],
      cp_approved_date: [''],
      cp_application: [''],
      cp_conn_date: [''],
      cp_trnsform_install: [''],
      cp_trnsform_size: [''],
      cp_trnsform_rating: ['', Validators.compose([Validators.pattern('[0-9. ]*'), Validators.required])],
      cp_trnsform_sl_kva: ['', Validators.compose([Validators.pattern('[0-9. ]*'), Validators.required])],
      cp_trnsform_sl_kw: ['', Validators.compose([Validators.pattern('[0-9. ]*'), Validators.required])],
      cp_avail_vs: [''],
      cp_threephase_avail: [''],
      cp_type_conn: [''],
      cp_tariff: [''],
      cp_tariff_id: [''],
      cp_vol_l1: ['', Validators.compose([Validators.pattern('[0-9. ]*'), Validators.required])],
      cp_vol_l2: ['', Validators.compose([Validators.pattern('[0-9. ]*'), Validators.required])],
      cp_vol_l3: ['', Validators.compose([Validators.pattern('[0-9. ]*'), Validators.required])],
      cp_vol_ng: ['', Validators.compose([Validators.pattern('[0-9. ]*'), Validators.required])],
      cp_annave_opd: ['', Validators.compose([Validators.pattern('[0-9. ]*'), Validators.required])],
      cp_avg_mb: ['', Validators.compose([Validators.pattern('[0-9. ]*'), Validators.required])]
    });

    this.storage.get('BasicData').then((basicdata) => {
      this.basicdata = basicdata;  
      this.siteForm.patchValue({
        cp_status:basicdata.cp_status,
        cp_electrical_company:basicdata.cp_electrical_company,
        cp_wapda:basicdata.cp_wapda,
        cp_grid_station_loc:basicdata.cp_grid_station_loc,
        cp_availab:basicdata.cp_availab,
        cp_con_meter:basicdata.cp_con_meter,
        cp_energy_meter_type:basicdata.cp_energy_meter_type,
        cp_application_date:basicdata.cp_application_date,
        cp_approved_date:basicdata.cp_approved_date,
        cp_application:basicdata.cp_application,
        cp_conn_date:basicdata.cp_conn_date,
        cp_trnsform_install:basicdata.cp_trnsform_install,
        cp_trnsform_size:basicdata.cp_trnsform_size,
        cp_trnsform_rating:basicdata.cp_trnsform_rating,
        cp_trnsform_sl_kva:basicdata.cp_trnsform_sl_kva,
        cp_trnsform_sl_kw:basicdata.cp_trnsform_sl_kw,
        cp_avail_vs:basicdata.cp_avail_vs,
        cp_threephase_avail:basicdata.cp_threephase_avail,
        cp_type_conn:basicdata.cp_type_conn,
        cp_tariff:basicdata.cp_tariff,
        cp_tariff_id:basicdata.cp_tariff_id,
        cp_vol_l1:basicdata.cp_vol_l1,
        cp_vol_l2:basicdata.cp_vol_l2,
        cp_vol_l3:basicdata.cp_vol_l3,
        cp_vol_ng:basicdata.cp_vol_ng,
        cp_annave_opd:basicdata.cp_annave_opd,
        cp_avg_mb:basicdata.cp_avg_mb

        
        })
       
    });
    this.storage.get('Survey').then((survey) => {
    });
  }
  submit(siteForm) {
    this.odooRPC.call('telco.tower.site', 'write', [[this.basicdata.id], siteForm], {}).then(res => {
      let toast = this.toastCtrl.create({
        message: 'Update sucess',
        duration: 3000,
        position: 'bottom'
      });
      toast.present();
      // toster all is ok and update the parrent with an event
    }).catch(err => {
      // alert some kind of errpr
       let alert = this.alertCtrl.create({
        title: 'Failed To Sumbit',
        subTitle: 'Please Try Again: ' + (err.message || this.parse.getError(err)),
        buttons: ['Dismiss']
      });
      alert.present();
    });
  }
  ionViewDidLoad() {
  }

}
