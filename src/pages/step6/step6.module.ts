import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Step6Page } from './step6';
import { NextPageModule } from '../../components/next-page/next-page.module';
import { OdooDropDownModule } from '../../components/dropdown/dropdown.module';
import { AutoCompleteModule } from '../../components/autocomplete/autocomplete.module';
import { SubmitModule } from '../../components/submit/submit.module';
import { SurveyModule } from '../../components/survey/survey.module';

@NgModule({
  declarations: [
    Step6Page,
  ],
  imports: [
    IonicPageModule.forChild(Step6Page),
    NextPageModule,
    OdooDropDownModule,
    AutoCompleteModule,
    SubmitModule,
    SurveyModule
  ],
})
export class Step6PageModule {}
