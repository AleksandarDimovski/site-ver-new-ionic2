import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, Events } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { FormGroup, FormBuilder } from '@angular/forms';
import { OdooRPCService } from "../../services/jsonRpc";
import { AlertController, LoadingController } from 'ionic-angular';
import { ParseError } from "../../services/parseError";

/**
 * Generated class for the Step6Page page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-step6',
  templateUrl: 'step6.html',
})
export class Step6Page {

  basicdata:any;
  siteForm: FormGroup;
  loc: any = [];

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams, 
    private storage: Storage,
    private odooRPC: OdooRPCService, 
    public formBuilder: FormBuilder,
    private alertCtrl: AlertController,
    private toastCtrl: ToastController,
    public events: Events,
    private parse: ParseError,
  ) {
    
    this.siteForm = this.formBuilder.group({
      sap_status: [false],
      sap_ogh: [false],
      sap_seccompany_id: [''],
      sap_seccompany_contact: [''],
      sap_seccompany_gdc: [''],
      sap_seccompany_gnc: [''],
      sap_bwallc: [''],
      sap_gate_install: [''],
      sap_fence_install: [''],
      sap_independent_access: [false],
      sap_24x7access: [false],
      sap_bw_constructed: [false]
    });

    this.storage.get('BasicData').then((basicdata) => {
      this.basicdata = basicdata;  
      this.siteForm.patchValue({
        sap_status:basicdata.sap_status,
        sap_ogh:basicdata.sap_ogh,
        sap_seccompany_id:basicdata.sap_seccompany_id,
        sap_seccompany_contact:basicdata.sap_seccompany_contact,
        sap_seccompany_gdc:basicdata.sap_seccompany_gdc,
        sap_seccompany_gnc:basicdata.sap_seccompany_gnc,
        sap_bwallc:basicdata.sap_bwallc,
        sap_gate_install:basicdata.sap_gate_install,
        sap_fence_install:basicdata.sap_fence_install,
        sap_independent_access:basicdata.sap_independent_access,
        sap_24x7access:basicdata.sap_24x7access,
        sap_bw_constructed:basicdata.sap_bw_constructed

        
        })
       
    });
    this.storage.get('Survey').then((survey) => {
    });
  }
  submit(siteForm) {
    this.odooRPC.call('telco.tower.site', 'write', [[this.basicdata.id], siteForm], {}).then(res => {
      let toast = this.toastCtrl.create({
        message: 'Update sucess',
        duration: 3000,
        position: 'bottom'
      });
      toast.present();
      // toster all is ok and update the parrent with an event
    }).catch(err => {
      // alert some kind of errpr
       let alert = this.alertCtrl.create({
        title: 'Failed To Sumbit',
        subTitle: 'Please Try Again: ' + (err.message || this.parse.getError(err)),
        buttons: ['Dismiss']
      });
      alert.present();
    });
  }
  ionViewDidLoad() {
  }

}
