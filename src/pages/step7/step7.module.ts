import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Step7Page } from './step7';
import { NextPageModule } from '../../components/next-page/next-page.module';
import { OdooDropDownModule } from '../../components/dropdown/dropdown.module';
import { AutoCompleteModule } from '../../components/autocomplete/autocomplete.module';
import { SubmitModule } from '../../components/submit/submit.module';
import { SurveyModule } from '../../components/survey/survey.module';

@NgModule({
  declarations: [
    Step7Page,
  ],
  imports: [
    IonicPageModule.forChild(Step7Page),
    NextPageModule,
    OdooDropDownModule,
    AutoCompleteModule,
    SubmitModule,
    SurveyModule
  ],
})
export class Step7PageModule {}
