import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, Events } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { FormGroup, FormBuilder } from '@angular/forms';
import { OdooRPCService } from "../../services/jsonRpc";
import { AlertController, LoadingController } from 'ionic-angular';
import { ParseError } from "../../services/parseError";

/**
 * Generated class for the Step7Page page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-step7',
  templateUrl: 'step7.html',
})
export class Step7Page {

  basicdata:any;
  siteForm: FormGroup;
  loc: any = [];

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams, 
    private storage: Storage,
    private odooRPC: OdooRPCService, 
    public formBuilder: FormBuilder,
    private alertCtrl: AlertController,
    private toastCtrl: ToastController,
    public events: Events,
    private parse: ParseError,
  ) {
    
    this.siteForm = this.formBuilder.group({
      site_lease_id: [false]
    });

    this.storage.get('BasicData').then((basicdata) => {
      this.basicdata = basicdata;  
      this.siteForm.patchValue({
        site_lease_id:basicdata.site_lease_id

        })
       
    });
    this.storage.get('Survey').then((survey) => {
    });
  }
  submit(siteForm) {
    this.odooRPC.call('telco.tower.site', 'write', [[this.basicdata.id], siteForm], {}).then(res => {
      let toast = this.toastCtrl.create({
        message: 'Update sucess',
        duration: 3000,
        position: 'bottom'
      });
      toast.present();
      // toster all is ok and update the parrent with an event
    }).catch(err => {
      // alert some kind of errpr
       let alert = this.alertCtrl.create({
        title: 'Failed To Sumbit',
        subTitle: 'Please Try Again: ' + (err.message || this.parse.getError(err)),
        buttons: ['Dismiss']
      });
      alert.present();
    });
  }
  ionViewDidLoad() {
  }

}

