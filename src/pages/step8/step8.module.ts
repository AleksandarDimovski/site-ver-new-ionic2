import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Step8Page } from './step8';
import { NextPageModule } from '../../components/next-page/next-page.module';
import { OdooDropDownModule } from '../../components/dropdown/dropdown.module';
import { AutoCompleteModule } from '../../components/autocomplete/autocomplete.module';
import { SubmitModule } from '../../components/submit/submit.module';
import { EquipmentBasicTemplateModule } from '../../components/equipment-basic-template/equipment-basic-template.module';
import { SurveyModule } from '../../components/survey/survey.module';


@NgModule({
  declarations: [
    Step8Page,
  ],
  imports: [
    IonicPageModule.forChild(Step8Page),
    NextPageModule,
    OdooDropDownModule,
    AutoCompleteModule,
    SubmitModule,
    EquipmentBasicTemplateModule,
    SurveyModule
  ],
})
export class Step8PageModule {}
