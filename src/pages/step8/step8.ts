import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, Events } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { OdooRPCService } from "../../services/jsonRpc";
import { AlertController } from 'ionic-angular';
import { ParseError } from "../../services/parseError";
import _  from 'underscore';

/**
 * Generated class for the Step8Page page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-step8',
  templateUrl: 'step8.html',
})
export class Step8Page {
  public hideShowContent: boolean = false;
  public hideShowSubContent: boolean = false;
  equipment:any;
  equip:any;
  siteForm: FormGroup;
  name: string = '';

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams, 
    private storage: Storage,
    private odooRPC: OdooRPCService, 
    public formBuilder: FormBuilder,
    private alertCtrl: AlertController,
    private toastCtrl: ToastController,
    public events: Events,
    private parse: ParseError,
  ) {
    
    this.siteForm = this.formBuilder.group({
      gs_installdate: [''],
      gs_kva_rating: ['', Validators.compose([Validators.pattern('[0-9. ]*'), Validators.required])],
      gs_kva_actual: ['', Validators.compose([Validators.pattern('[0-9. ]*'), Validators.required])],
      gs_fonload: ['', Validators.compose([Validators.pattern('[0-9. ]*'), Validators.required])],
      gs_montly_rh: ['', Validators.compose([Validators.pattern('[0-9. ]*'), Validators.required])],
      gs_acum__rh: ['', Validators.compose([Validators.pattern('[0-9. ]*'), Validators.required])],
      gs_fuel_tank: [false],
      gs_fuel_tank_l: ['', Validators.compose([Validators.pattern('[0-9. ]*'), Validators.required])],
      gs_fuel_avgcons: ['', Validators.compose([Validators.pattern('[0-9. ]*'), Validators.required])],
      gs_fuel_mc: ['', Validators.compose([Validators.pattern('[0-9. ]*'), Validators.required])],
      gs_canoy: [false],
      gs_inroom: [false],
      gs_canopy_ins: [false],
      gs_cmodel: [''],
      gs_hmr: [''],
      gs_panel_sn: [''],
      gs_engine_sn: [''],
      gs_ats: [false],
      gs_ats_breaker: [false],
      gs_ats_panel: [false],
      gs_ats_cabling: [false],
      gs_alarm: [false],
      gs_cleaning: [false],
      gs_paint: [false],
      gs_lastoilcange: [''],
      gs_lastprh: [''],

    });
    this.storage.get('Equipment').then((equipment) => {
      let equip = _.where(equipment, {type: 'generator'} );
      this.equip = equip;
      console.log(this.equip);
      
      for (let i in this.equip) {
        this.name = equip[i].name ;
        this.siteForm.patchValue({
                      real_equipment_id: equip[i].real_equipment_id,
                      gs_installdate: equip[i].gs_installdate,
                      gs_kva_rating: equip[i].gs_kva_rating,
                      gs_kva_actual: equip[i].gs_kva_actual,
                      gs_fonload: equip[i].gs_fonload,
                      gs_montly_rh: equip[i].gs_montly_rh,
                      gs_acum__rh: equip[i].gs_acum__rh,
                      gs_fuel_tank:equip[i].gs_fuel_tank,
                      gs_fuel_tank_l: equip[i].gs_fuel_tank_l,
                      gs_fuel_avgcons: equip[i].gs_fuel_avgcons,
                      gs_fuel_mc: equip[i].gs_fuel_mc,
                      gs_canoy: equip[i].gs_canoy,
                      gs_inroom: equip[i].gs_inroom,
                      gs_canopy_ins: equip[i].gs_canopy_ins,
                      gs_cmodel: equip[i].gs_cmodel,
                      gs_hmr: equip[i].gs_hmr,
                      gs_panel_sn: equip[i].gs_panel_sn,
                      gs_engine_sn: equip[i].gs_engine_sn,
                      gs_ats: equip[i].gs_ats,
                      gs_ats_breaker: equip[i].gs_ats_breaker,
                      gs_ats_panel: equip[i].gs_ats_panel,
                      gs_ats_cabling: equip[i].gs_ats_cabling,
                      gs_alarm: equip[i].gs_alarm,
                      gs_cleaning: equip[i].gs_cleaning,
                      gs_paint: equip[i].gs_paint,
                      gs_lastoilcange: equip[i].gs_lastoilcange,
                      gs_lastprh: equip[i].gs_lastprh
                       
    });

    console.log(equip[i].name)
    }
    });
    this.storage.get('Survey').then((survey) => {
      console.log(survey)
    });
  }
  submit(siteForm) {
    console.log(this.equipment[0].id);
    this.odooRPC.call('telco.equipment', 'write', [[this.equipment[0].id], siteForm], {}).then(res => {
      let toast = this.toastCtrl.create({
        message: 'Update sucess',
        duration: 3000,
        position: 'bottom'
      });
      toast.present();
      // toster all is ok and update the parrent with an event
    }).catch(err => {
      // alert some kind of errpr
       let alert = this.alertCtrl.create({
        title: 'Failed To Sumbit',
        subTitle: 'Please Try Again: ' + (err.message || this.parse.getError(err)),
        buttons: ['Dismiss']
      });
      alert.present();
    });
  }
  public hideShowContentButton() {
    this.hideShowContent = !this.hideShowContent;
}
public hideShowContentSubButton() {
  this.hideShowSubContent = !this.hideShowSubContent;
}
  ionViewDidLoad() {
  }

}
