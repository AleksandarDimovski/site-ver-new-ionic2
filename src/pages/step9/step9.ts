import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, Events } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { OdooRPCService } from "../../services/jsonRpc";
import { AlertController } from 'ionic-angular';
import { ParseError } from "../../services/parseError";
import  _  from 'underscore';

/**
 * Generated class for the Step9Page page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-step9',
  templateUrl: 'step9.html',
})
export class Step9Page {
  public hideShowContent: boolean = false;
  public hideShowSubContent: boolean = false;
  equipment:any;
  equip:any;
  siteForm: FormGroup;
  name: string = '';

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams, 
    private storage: Storage,
    private odooRPC: OdooRPCService, 
    public formBuilder: FormBuilder,
    private alertCtrl: AlertController,
    private toastCtrl: ToastController,
    public events: Events,
    private parse: ParseError,
  ) {
    
    this.siteForm = this.formBuilder.group({
      shelter_type_id: [''],
      shelter_plot_size: ['', Validators.compose([Validators.pattern('[0-9. ]*'), Validators.required])],
      shelter_room: [false],
      shelter_room_number: ['', Validators.compose([Validators.pattern('[0-9. ]*'), Validators.required])],
      shelter_room_body: [false],
      shelter_room_dim: [''],
      shelter_door: [false],
      shelter_lock: [false],
      shelter_cabel_tray: [false],
      shelter_earthing_cable: [false],
      shelter_cover_cabel: [false],
      shelter_fire_cbox: [false],
      shelter_check_fire_cbox: [false],
      shelter_check_light: [false],
      shelter_cut_power: [false],
      shelter_siren_test: [false],
      shelter_wleader: [false],

    });
    //var equip = _.where(eqStorage.getInfo(), {type: 'shelter'});

    
    this.storage.get('Equipment').then((equipment) => {
      let equip = _.where(equipment, {type: 'shelter'} );
      this.equip = equip;
      console.log(this.equip);
      
      for (let i in this.equip) {
        this.name = equip[i].name ;
        this.siteForm.patchValue({
           real_equipment_id: equip[i].real_equipment_id,
           shelter_type_id: equip[i].shelter_type_id,
           shelter_plot_size: equip[i].shelter_plot_size,
           shelter_room: equip[i].shelter_room,
           shelter_room_number: equip[i].shelter_room_number,
           shelter_room_body: equip[i].shelter_room_body,
           shelter_room_dim: equip[i].shelter_room_dim,
           shelter_door: equip[i].shelter_door,
           shelter_lock: equip[i].shelter_lock,
           shelter_cabel_tray: equip[i].shelter_cabel_tray,
           shelter_earthing_cable: equip[i].shelter_earthing_cable,
           shelter_cover_cabel: equip[i].shelter_cover_cabel,
           shelter_fire_cbox: equip[i].shelter_fire_cbox,
           shelter_check_fire_cbox: equip[i].shelter_check_fire_cbox,
           shelter_check_light: equip[i].shelter_check_light,
           shelter_cut_power: equip[i].shelter_cut_power,
           shelter_siren_test: equip[i].shelter_siren_test,
           shelter_wleader: equip[i].shelter_wleader,
        });
    }
    
    });
    this.storage.get('Survey').then((survey) => {
      console.log(survey)
    });
  }
  submit(siteForm) {
    console.log(this.equipment[0].id);
    this.odooRPC.call('telco.equipment', 'write', [[this.equipment[0].id], siteForm], {}).then(res => {
      let toast = this.toastCtrl.create({
        message: 'Update sucess',
        duration: 3000,
        position: 'bottom'
      });
      toast.present();
      // toster all is ok and update the parrent with an event
    }).catch(err => {
      // alert some kind of errpr
       let alert = this.alertCtrl.create({
        title: 'Failed To Sumbit',
        subTitle: 'Please Try Again: ' + (err.message || this.parse.getError(err)),
        buttons: ['Dismiss']
      });
      alert.present();
    });
  }
  public hideShowContentButton() {
    this.hideShowContent = !this.hideShowContent;
}
public hideShowContentSubButton() {
  this.hideShowSubContent = !this.hideShowSubContent;
}
  ionViewDidLoad() {
  }

}
