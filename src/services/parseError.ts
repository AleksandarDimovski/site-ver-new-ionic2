import { Injectable } from '@angular/core';

@Injectable()
export class ParseError {
// DataError: UserError
    getError(string) {
        if (typeof string == "string") {
            if (string.includes('ValidationError')) {
                return string.substr(string.indexOf("ValidationError"));
            }else if (string.includes('TypeError:')){
                return string.substr(string.indexOf("TypeError:"));
            }else if (string.includes('except_orm:')){
                return string.substr(string.indexOf("except_orm:"));
            }else if (string.includes('DataError:')){
                return string.substr(string.indexOf("DataError:"));
            }else if (string.includes('UserError:')){
                return string.substr(string.indexOf("UserError:"));
            }
            else {
                return string;
            }

        }


    }



}